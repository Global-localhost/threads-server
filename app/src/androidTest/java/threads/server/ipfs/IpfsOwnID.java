package threads.server.ipfs;


import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.LogUtils;

import static junit.framework.TestCase.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class IpfsOwnID {
    private static final String TAG = IpfsOwnID.class.getSimpleName();

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void dummy() {
        assertNotNull(context);
    }

    //@Test
    public void id() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        LogUtils.error(TAG, "PID : " + Objects.requireNonNull(IPFS.getPID(context)).getPid());
        String lastString = "";
        AtomicBoolean found = new AtomicBoolean(false);

        while (!found.get()) {
            PeerInfo info = ipfs.id();
            assertNotNull(info);

            String addresses = info.getMultiAddresses().toString();
            if (!lastString.equals(addresses)) {
                LogUtils.error(TAG, addresses);
                LogUtils.error(TAG, "Swarm size : " + ipfs.swarmPeers().size());
                lastString = addresses;
            }

            Thread.sleep(10000);
        }
    }


}

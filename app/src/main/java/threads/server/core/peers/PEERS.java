package threads.server.core.peers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class PEERS extends PeersAPI {

    private static final Migration MIGRATION_112_113 = new Migration(112, 113) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE User "
                    + " ADD COLUMN work TEXT");
        }
    };
    private static final Migration MIGRATION_113_114 = new Migration(113, 114) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE User "
                    + " ADD COLUMN timestamp INTEGER DEFAULT 0 NOT NULL");
        }
    };
    private static final Migration MIGRATION_114_115 = new Migration(114, 115) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE User "
                    + " ADD COLUMN visible INTEGER DEFAULT 1 NOT NULL");
        }
    };
    private static PEERS INSTANCE = null;

    private PEERS(final PEERS.Builder builder) {
        super(builder.usersDatabase);
    }

    @NonNull
    private static PEERS createPeers(@NonNull UsersDatabase usersDatabase) {

        return new Builder()
                .usersDatabase(usersDatabase)
                .build();
    }

    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    UsersDatabase usersDatabase = Room.databaseBuilder(context, UsersDatabase.class,
                            UsersDatabase.class.getSimpleName()).
                            addMigrations(MIGRATION_112_113, MIGRATION_113_114, MIGRATION_114_115).
                            fallbackToDestructiveMigration().build();


                    INSTANCE = PEERS.createPeers(usersDatabase);
                }
            }
        }
        return INSTANCE;
    }


    static class Builder {
        UsersDatabase usersDatabase = null;

        PEERS build() {

            return new PEERS(this);
        }


        Builder usersDatabase(@NonNull UsersDatabase usersDatabase) {

            this.usersDatabase = usersDatabase;
            return this;
        }
    }
}

package threads.server.ipfs;

@SuppressWarnings("WeakerAccess")
public interface Closeable {
    boolean isClosed();

    boolean isOffline();
}

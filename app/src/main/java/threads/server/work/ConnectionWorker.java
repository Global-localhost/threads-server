package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import threads.LogUtils;
import threads.server.services.ConnectService;

public class ConnectionWorker extends Worker {


    private static final String TAG = ConnectionWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public ConnectionWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    public static OneTimeWorkRequest getWork() {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        return new OneTimeWorkRequest.Builder(ConnectionWorker.class)
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();

    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");
        try {

            ConnectService.connect(getApplicationContext());

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}


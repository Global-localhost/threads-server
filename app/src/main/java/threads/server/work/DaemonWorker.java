package threads.server.work;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import threads.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.ipfs.IPFS;
import threads.server.ipfs.Reachable;
import threads.server.utils.Network;

public class DaemonWorker extends Worker {
    private static final String TAG = DaemonWorker.class.getSimpleName();
    private final IPFS ipfs;

    private final AtomicReference<Notification> mLastNotification = new AtomicReference<>(null);
    private final NotificationManager mNotificationManager;


    @SuppressWarnings("WeakerAccess")
    public DaemonWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        ipfs = IPFS.getInstance(context);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork() {

        return new OneTimeWorkRequest.Builder(DaemonWorker.class)
                .addTag(TAG)
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();

    }


    public static void replace(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork());
    }

    private void createChannel(@NonNull Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                CharSequence name = context.getString(R.string.daemon_channel_name);
                String description = context.getString(R.string.daemon_channel_description);
                NotificationChannel mChannel = new NotificationChannel(TAG, name,
                        NotificationManager.IMPORTANCE_LOW);

                mChannel.setDescription(description);

                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, "" + e.getLocalizedMessage(), e);
            }
        }
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {

            createChannel(getApplicationContext());

            ForegroundInfo foregroundInfo = createForegroundInfo();
            setForegroundAsync(foregroundInfo);

            boolean network = Network.isConnected(getApplicationContext());
            while (!isStopped()) {

                long seeded = ipfs.getSeeded();
                Reachable reachable = ipfs.getReachable();

                    Notification notification =
                            createNotification((int) ipfs.getSwarmPort(),
                                    ipfs.numSwarmPeers(), reachable, seeded);

                    if (mNotificationManager != null) {
                        mNotificationManager.notify(TAG.hashCode(), notification);
                    }


                java.lang.Thread.sleep(1000);

                if (!network) {
                    // now we are connected again
                    if (Network.isConnected(getApplicationContext())) {
                        WorkManager.getInstance(getApplicationContext())
                                .beginWith(BootstrapWorker.getWork())
                                .then(ConnectionWorker.getWork()).enqueue();
                    }
                }

                network = Network.isConnected(getApplicationContext());
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            closeNotification();
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


    private void closeNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(TAG.hashCode());
        }
    }

    @Override
    public void onStopped() {
        super.onStopped();
        closeNotification();
    }

    @NonNull
    private ForegroundInfo createForegroundInfo() {
        Notification notification = createNotification(
                (int) ipfs.getSwarmPort(), ipfs.numSwarmPeers(), ipfs.getReachable(), ipfs.getSeeded());
        mLastNotification.set(notification);
        return new ForegroundInfo(TAG.hashCode(), notification);
    }

    private Notification createNotification(int port, long peers, @NonNull Reachable reachable, long seeded) {

        Notification.Builder builder;
        if (mLastNotification.get() != null) {
            builder = Notification.Builder.recoverBuilder(
                    getApplicationContext(), mLastNotification.get());
        } else {
            builder = new Notification.Builder(getApplicationContext(), TAG);
        }

        builder.setSmallIcon(R.drawable.access_point_network);


        RemoteViews remoteView = new RemoteViews(
                getApplicationContext().getPackageName(), R.layout.notification);

        if (reachable == Reachable.PUBLIC) {

            remoteView.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_on);
            String info = getApplicationContext().getString(R.string.reachable_public);
            remoteView.setTextViewText(R.id.notification_info, info);
        } else if (reachable == Reachable.PRIVATE) {

            remoteView.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_off);
            String info = getApplicationContext().getString(R.string.reachable_private);
            if (!Network.isConnected(getApplicationContext())) {
                info = getApplicationContext().getString(R.string.reachable_network_down);
            } else {
                if (Network.isConnectedMobile(getApplicationContext())) {
                    info = getApplicationContext().getString(R.string.reachable_private_mobile);
                }
            }
            remoteView.setTextViewText(R.id.notification_info, info);
        } else {

            remoteView.setImageViewResource(R.id.action_turn_off, R.drawable.access_point_network_unknown);
            String info = getApplicationContext().getString(R.string.reachable_unknown);
            remoteView.setTextViewText(R.id.notification_info, info);
        }

        remoteView.setTextViewText(R.id.notification_text_content,
                getApplicationContext().getString(R.string.daemon_port_title, String.valueOf(port)));
        builder.setStyle(new Notification.DecoratedCustomViewStyle());
        builder.setCustomContentView(remoteView);
        builder.setCustomBigContentView(remoteView);
        builder.setUsesChronometer(true);
        builder.setSubText("Peers [" + peers + "] " + getSeeding(getApplicationContext(), seeded));
        builder.setOngoing(true);

        PendingIntent stopIntent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());

        remoteView.setOnClickPendingIntent(R.id.action_turn_off, stopIntent);

        Intent defaultIntent = new Intent(getApplicationContext(), MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent defaultPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, defaultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(defaultPendingIntent);

        return builder.build();

    }

    private String getSeeding(@NonNull Context context, long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return context.getString(R.string.traffic, fileSize);
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return context.getString(R.string.traffic_kb, fileSize);
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return context.getString(R.string.traffic_mb, fileSize);
        }
    }

}

